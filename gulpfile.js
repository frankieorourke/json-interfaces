require("coffee-script/register");

var gulp =        require('gulp'),
	coffee =        require('gulp-coffee'),
	watch =         require('gulp-watch'),
	mocha =         require('gulp-mocha'),
	jade =          require('gulp-jade'),
	insert =        require('gulp-insert'),
	gutil =         require('gulp-util');

gulp.task('default', function () {
	return gulp.src('./spec/**/*-spec*', {read: false})
		.pipe(mocha({reporter: 'dot'}));
});

gulp.task('jade', function() {
	gulp.src('./src/templates/**/*.jade')
		.pipe(jade({
			client: true
		}))
		.pipe(insert.prepend("var jade = require(\"jade/runtime\");\n"))
		.pipe(insert.append("\nmodule.exports = template;"))
		.pipe(gulp.dest('./lib/templates'))
});

gulp.task('watch', function(){
	//initial transpiling
	gulp.start('coffee', 'jade')
	gulp.watch("src/templates/**/*.jade", ["jade"]);
	gulp.watch("src/**/*.coffee", ["coffee"]);
});

gulp.task('coffee', function() {
	gulp.src('./src/**/*.coffee')
		.pipe(coffee({bare: true}).on('error', gutil.log))
		.pipe(gulp.dest('./lib/'))
});