var $, Element, Events, async, validator, _;

Events = require('./events');

_ = require('underscore');

$ = require('jquery');

async = require('async');

validator = require('validator');

Element = (function() {
  Element.prototype.errors = [];

  Element.prototype.rendered = false;

  Element.prototype.noValue = false;

  Element.prototype.validator = validator;

  function Element(options) {
    var embeddedConfig;
    if (options == null) {
      options = {};
    }
    embeddedConfig = typeof this.getConfig === "function" ? this.getConfig() : {};
    this.options = _.extend(embeddedConfig, options);
    if (this.options.setValue !== false) {
      this.setValue(this.options.value);
    }
    if (!this.options.name) {
      throw new Error("name required");
    }
    this.initConditional();
    this.initValidation();
  }

  Element.prototype.defaultValue = function() {
    return this.options.defaultValue;
  };

  Element.prototype.initOptions = function(options) {
    if (this.options) {
      return this.options = _.extend(this.options, options);
    }
  };

  Element.prototype.submit = function(callback) {
    this.trigger("submit");
    if (this.noValue) {
      return;
    }
    return this.getValue((function(_this) {
      return function(value) {
        if (typeof callback === "function") {
          return callback.call(_this, value);
        }
      };
    })(this));
  };

  Element.prototype.getName = function() {
    return this.options.name;
  };

  Element.prototype.getValue = function(fresh, callback) {
    if (fresh == null) {
      fresh = false;
    }
    if (typeof callback !== "function") {
      callback = fresh;
      fresh = false;
    }
    if (fresh) {
      this.updateValue(true);
    }
    return callback.call(this, this.value);
  };

  Element.prototype.getValueForView = function(callback) {
    return callback.call(this, this.value);
  };

  Element.prototype.setValue = function(value, options) {
    var formattedValue;
    if (options == null) {
      options = {};
    }
    value = value || this.defaultValue();
    if (!options.silent) {
      options.silent = false;
    }
    formattedValue = this.formatValue(value);
    if (this.value === formattedValue) {
      return;
    }
    this.value = formattedValue;
    if (!(options.updateDependentValues === false || this.rendered === !true)) {
      this.updateDependentValues(options.silent);
    }
    if (!options.silent) {
      this.trigger("change", this.value, this.value);
    }
    return this.value;
  };

  Element.prototype.updateDependentValues = function(options) {};

  Element.prototype.isValid = function(callback) {
    return this.getErrors((function(_this) {
      return function() {
        var errorCount, errors, name, _ref;
        errorCount = 0;
        _ref = _this.errors;
        for (name in _ref) {
          errors = _ref[name];
          errorCount += (errors ? errors.length : 0);
        }
        return callback(errorCount === 0);
      };
    })(this));
  };

  Element.prototype.ifValid = function(callback) {
    if (this.errors.length === 0) {
      return callback.call(this);
    }
  };

  Element.prototype.ifInvalid = function(callback) {
    if (this.errors.length > 0) {
      return callback.call(this);
    }
  };

  Element.prototype.validateIsValid = function(callback) {
    return this.validate((function(_this) {
      return function() {
        if (_this.errors.length === 0) {
          return callback.call(_this);
        }
      };
    })(this));
  };

  Element.prototype.getErrors = function(callback) {
    var validatorCallback, validatorCallbacks, _i, _len, _ref;
    if (this.noValue) {
      return callback.call(this);
    }
    this.updateValue();
    if (this.options.validation.length > 0) {
      validatorCallbacks = [];
      _ref = this.options.validation;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        validator = _ref[_i];
        validatorCallback = ((function(_this) {
          return function(validator) {
            return function(asyncCallback) {
              return validator(_this.value, _this.validator, _this.options.parentElement, function(results) {
                if (results instanceof Array) {
                  return asyncCallback(null, results);
                } else if (results) {
                  return asyncCallback(null, [results]);
                } else {
                  return asyncCallback(null, []);
                }
              });
            };
          };
        })(this))(validator);
        validatorCallbacks.push(validatorCallback);
      }
      this.errors = [];
      return async.parallel(validatorCallbacks, (function(_this) {
        return function(err, results) {
          var result, _j, _len1;
          for (_j = 0, _len1 = results.length; _j < _len1; _j++) {
            result = results[_j];
            _this.errors = _this.errors.concat(result);
          }
          return callback.call(_this, _this.errors);
        };
      })(this));
    } else {
      return callback.call(this);
    }
  };

  Element.prototype.addError = function(error) {
    return this.errors.push(error);
  };

  Element.prototype.validate = function(callback) {
    throw new Error('Not Implemented');
  };

  Element.prototype.showErrors = function() {};

  Element.prototype.hideErrors = function() {};

  Element.prototype.clearErrors = function() {
    this.hideErrors(true);
    return this.errors = [];
  };

  Element.prototype.render = function($el) {
    if (typeof this.beforeRender === "function") {
      this.beforeRender();
    }
    if ($el) {
      this.options.$el = $el;
    }
    if (!this.options.$el) {
      throw new Error('@options.$el not set');
    }
    if (typeof this.options.template !== "function") {
      throw new Error('template required');
    }
    this.getValueForView(function(value) {
      return this.options.$el.html(this.options.template({
        value: value,
        options: this.options,
        element: this,
        namespace: this.options.namespace ? this.options.namespace + "-" : ""
      }));
    });
    this.rendered = true;
    this.updateDependentValues({
      silent: true
    });
    this.afterRender();
    return this.trigger("afterRender");
  };

  Element.prototype.updateValue = function(silent) {
    if (silent == null) {
      silent = false;
    }
  };

  Element.prototype.afterRender = function() {};

  Element.prototype.close = function() {
    if (this.layout) {
      return this.layout.close();
    }
  };

  Element.prototype.formatValue = function(value) {
    return value;
  };

  Element.prototype.conditionMet = function(callback) {
    if (typeof this.options.conditional !== "function") {
      return callback(null);
    }
    return this.options.conditional(this.value, this.options.parentElement, (function(_this) {
      return function(result) {
        return callback(result);
      };
    })(this));
  };

  Element.prototype.checkConditional = function(immediate) {
    if (immediate == null) {
      immediate = false;
    }
    return this.conditionMet((function(_this) {
      return function(result) {
        if (result === false) {
          return _this.hideElement(immediate, function() {
            return _this.clearErrors();
          });
        } else {
          return _this.showElement();
        }
      };
    })(this));
  };

  Element.prototype.showElement = function(callback) {
    if (!this.options.$el) {
      return;
    }
    if (this.showing) {
      return;
    }
    this.showing = true;
    this.hiding = false;
    return this.options.$el.stop().slideDown("slow", (function(_this) {
      return function() {
        _this.options.$el.css('height', 'auto');
        _this.showing = false;
        if (typeof callback === "function") {
          return callback.call(_this);
        }
      };
    })(this));
  };

  Element.prototype.hideElement = function(immediate, callback) {
    if (typeof immediate === "function") {
      callback = immediate;
    }
    if (!this.options.$el) {
      return;
    }
    if (immediate) {
      this.options.$el.hide();
      this.hiding = false;
      if (typeof callback === "function") {
        return callback.call(this);
      }
    }
    if (this.hiding) {
      return;
    }
    this.hiding = true;
    this.showing = false;
    return this.options.$el.stop().slideUp("slow", (function(_this) {
      return function() {
        _this.options.$el.css('height', 'auto');
        _this.hiding = false;
        if (typeof callback === "function") {
          return callback.call(_this);
        }
      };
    })(this));
  };

  Element.prototype.initConditional = function() {
    var conditionalValues;
    if (this.options.parentElement) {
      this.options.parentElement.on("change", (function(_this) {
        return function() {
          return _this.checkConditional();
        };
      })(this));
    }
    if (typeof this.options.conditional !== "object") {
      return;
    }
    if (!this.options.parentElement) {
      return;
    }
    conditionalValues = this.options.conditional;
    return this.options.conditional = function(value, parentElement, done) {
      var check, checks, i, result, v;
      result = true;
      checks = [];
      for (i in conditionalValues) {
        v = conditionalValues[i];
        check = (function(i, v) {
          return function(callback) {
            return parentElement.getElement(i).getValue(function(value) {
              return callback(null, value === v);
            });
          };
        })(i, v);
        checks.push(check);
      }
      return async.parallel(checks, function(err, results) {
        var _i, _len;
        for (_i = 0, _len = results.length; _i < _len; _i++) {
          result = results[_i];
          if (result === false) {
            return done(false);
          }
        }
        return done(true);
      });
    };
  };

  Element.prototype.initValidation = function() {
    var message;
    if (typeof this.options.validation === "function") {
      this.options.validation = [this.options.validation];
    } else if (!this.options.validation) {
      this.options.validation = [];
    }
    if (!this.options.required) {
      return;
    }
    message = this.options.required === true ? "This field is required." : this.options.required;
    return this.options.validation.push((function(_this) {
      return function(value, validator, parentElement, done) {
        return done(value === "" || value === null || value === void 0 ? [message] : void 0);
      };
    })(this));
  };

  return Element;

})();

_.extend(Element.prototype, Events);

module.exports = Element;
