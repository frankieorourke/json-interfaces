var $, Address, JsonInterfaces, _,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

$ = require('jquery');

JsonInterfaces = require('../json-interfaces');

_ = require('underscore');

Address = (function(_super) {
  __extends(Address, _super);

  function Address(options) {
    if (!options.layout) {
      options.layout = JsonInterfaces.layouts.templateLayout;
    }
    options.elements || (options.elements = {});
    options.template = JsonInterfaces.templates.address;
    options.namespace = options.namespace + "-" + options.name;
    Address.__super__.constructor.call(this, options);
    _.extend(options.elements, {
      address: {
        label: "Street Address",
        required: true
      },
      city: {
        label: "City",
        required: true
      },
      state: {
        type: JsonInterfaces.elements.StatesField,
        label: "State",
        required: true
      },
      zip: {
        label: "Zip",
        required: true
      }
    });
    Address.__super__.constructor.call(this, options);
  }

  return Address;

})(JsonInterfaces.JsonElement);

module.exports = Address;
