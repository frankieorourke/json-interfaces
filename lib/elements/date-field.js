var DateField, JsonInterfaces, TextField,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

JsonInterfaces = require("../json-interfaces");

TextField = require("./text-field");

require('date-utils');

DateField = (function(_super) {
  __extends(DateField, _super);

  function DateField(options) {
    if (!options.template) {
      options.template = JsonInterfaces.templates.dateField;
    }
    DateField.__super__.constructor.call(this, options);
  }

  DateField.prototype.setValue = function(value, options) {
    if (options == null) {
      options = {};
    }
    return DateField.__super__.setValue.call(this, this.stringToDate(value), options);
  };

  DateField.prototype.formatValue = function(value) {
    return this.stringToDate(value);
  };

  DateField.prototype.formattedValueForView = function() {
    if (!this.value) {
      return '';
    }
    return this.value.toFormat("YYYY-MM-DD");
  };

  DateField.prototype.stringToDate = function(value) {
    var date;
    if (typeof value === "string") {
      date = new Date(value);
      value = isNaN(date) ? null : date;
    }
    return value;
  };

  DateField.prototype.setValueOnEl = function() {
    return this.input().attr("value", this.formattedValueForView());
  };

  return DateField;

})(TextField);

module.exports = DateField;
