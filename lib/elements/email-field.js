var EmailField, JsonInterfaces, TextField,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

JsonInterfaces = require("../json-interfaces");

TextField = require("./text-field");

EmailField = (function(_super) {
  __extends(EmailField, _super);

  function EmailField(options) {
    if (!options.template) {
      options.template = JsonInterfaces.templates.emailField;
    }
    EmailField.__super__.constructor.call(this, options);
    this.options.validation.push(function(value, validator, parentElement, done) {
      return done(!validator.isEmail(value) ? "Valid email required." : void 0);
    });
  }

  return EmailField;

})(TextField);

module.exports = EmailField;
