var Element, Html, JsonInterfaces,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

JsonInterfaces = require("../json-interfaces");

Element = require("../element");

Html = (function(_super) {
  __extends(Html, _super);

  Html.prototype.noValue = true;

  function Html(options) {
    this.options = options;
    this.options.html = options.html;
    this.options.template = JsonInterfaces.templates.html;
  }

  Html.prototype.getName = function() {
    return this.options.name;
  };

  Html.prototype.render = function($el) {
    if ($el) {
      this.options.$el = $el;
    }
    if (!this.options.$el) {
      throw new Error('@options.$el not set');
    }
    this.options.$el.html(this.options.template({
      options: this.options
    }));
    if (typeof this.afterRender === "function") {
      this.afterRender();
    }
    return this.trigger("afterRender");
  };

  Html.prototype.close = function() {
    return this.options.$el.empty();
  };

  Html.prototype.addClass = function() {};

  Html.prototype.removeClass = function() {};

  return Html;

})(Element);

module.exports = Html;
