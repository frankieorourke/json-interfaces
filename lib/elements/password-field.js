var PasswordField, TextField, passwordTemplate,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

TextField = require("./text-field");

passwordTemplate = require("../templates/password-field");

PasswordField = (function(_super) {
  __extends(PasswordField, _super);

  function PasswordField(options) {
    if (options == null) {
      options = {};
    }
    if (!options.template) {
      options.template = passwordTemplate;
    }
    PasswordField.__super__.constructor.call(this, options);
  }

  return PasswordField;

})(TextField);

module.exports = PasswordField;
