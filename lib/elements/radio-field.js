var $, JsonInterfaces, RadioField, UIElement,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

JsonInterfaces = require("../json-interfaces");

UIElement = require("./ui-element");

$ = require('jquery');

RadioField = (function(_super) {
  __extends(RadioField, _super);

  function RadioField(options) {
    if (options == null) {
      options = {};
    }
    if (!options.template) {
      options.template = JsonInterfaces.templates.radioField;
    }
    RadioField.__super__.constructor.call(this, options);
    if (!(this.options.choices || this.options.boolean)) {
      throw new Error("Choices required for field: " + this.getName());
    }
    if (this.options.boolean === true) {
      this.options.value = this.formatValue(this.options.value);
    }
    this.initChoices();
  }

  RadioField.prototype.initChoices = function() {
    var choice, choices, formattedChoices, parts, _i, _len;
    if (this.options.boolean === true) {
      this.options.choices = [
        {
          label: "Yes",
          value: "true"
        }, {
          label: "No",
          value: "false"
        }
      ];
      return;
    }
    if (typeof this.options.choices !== "string") {
      return;
    }
    formattedChoices = [];
    choices = this.options.choices.split(',');
    for (_i = 0, _len = choices.length; _i < _len; _i++) {
      choice = choices[_i];
      parts = choice.split(':');
      formattedChoices.push({
        label: parts[0],
        value: parts[1]
      });
    }
    return this.options.choices = formattedChoices;
  };

  RadioField.prototype.formatValue = function(value) {
    if (this.options.boolean === true) {
      if (value === "true") {
        return true;
      }
      if (value === "false") {
        return false;
      }
    }
    return value;
  };

  RadioField.prototype.formattedValueForView = function(value) {
    if (this.options.boolean === true) {
      if (value === true || value === "true") {
        return "true";
      }
      if (("false" ? value === false || value : void 0)) {
        return "false";
      }
    }
    return value;
  };

  RadioField.prototype.updateValue = function() {
    return this.setValue(this.readValueFromEl(), {
      updateDependentValues: false
    });
  };

  RadioField.prototype.updateDependentValues = function(options) {
    if (options == null) {
      options = {};
    }
    return this.setValueOnEl();
  };

  RadioField.prototype.afterRender = function() {
    return this.input().on("change", (function(_this) {
      return function() {
        _this.updateValue();
        return _this.ifValid(function() {
          return _this.hideErrors();
        });
      };
    })(this));
  };

  RadioField.prototype.readValueFromEl = function() {
    return $("input[name='" + this.getName() + "']:checked", this.options.$el).val();
  };

  RadioField.prototype.setValueOnEl = function() {
    return this.input().each((function(_this) {
      return function(i, el) {
        if ($(el).val() === _this.formattedValueForView(_this.value)) {
          return $(el).prop('checked', true);
        } else {
          return $(el).prop('checked', false);
        }
      };
    })(this));
  };

  RadioField.prototype.input = function() {
    if (!this.options.$el) {
      throw new Error('$el is required');
    }
    return $("input[name='" + this.elId() + "']", this.options.$el);
  };

  RadioField.prototype.removeListeners = function() {
    return this.input().unbind();
  };

  return RadioField;

})(UIElement);

module.exports = RadioField;
