var $, FormElement, Radio, jsoni,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

FormElement = require('../form-element');

$ = require('jquery');

jsoni = require('../jsoni');

Radio = (function(_super) {
  __extends(Radio, _super);

  function Radio(options) {
    options || (options = {});
    if (!options.template) {
      options.template = jsoni.getTemplate('radio');
    }
    options.value = options.value || '';
    options.label = options.label || '';
    options.help = options.help || '';
    Radio.__super__.constructor.call(this, options);
    if (!(this.options.choices || this.options.boolean)) {
      throw new Error("Choices required for field: " + this.getName());
    }
    if (this.options.boolean === true) {
      this.options.value = this.formatValue(this.options.value);
    }
    this.initChoices();
  }

  Radio.prototype.initChoices = function() {
    var choice, choices, formattedChoices, parts, _i, _len;
    if (this.options.boolean === true) {
      this.options.choices = [
        {
          label: "Yes",
          value: "true"
        }, {
          label: "No",
          value: "false"
        }
      ];
      return;
    }
    if (typeof this.options.choices !== "string") {
      return;
    }
    formattedChoices = [];
    choices = this.options.choices.split(',');
    for (_i = 0, _len = choices.length; _i < _len; _i++) {
      choice = choices[_i];
      parts = choice.split(':');
      formattedChoices.push({
        label: parts[0],
        value: parts[1]
      });
    }
    return this.options.choices = formattedChoices;
  };

  Radio.prototype.formatValue = function(value) {
    if (this.options.boolean === true) {
      if (value === "true") {
        return true;
      }
      if (value === "false") {
        return false;
      }
    }
    return value;
  };

  Radio.prototype.formatValueForView = function(value) {
    if (this.options.boolean === true) {
      if (value === true || value === "true") {
        return "true";
      }
      if (("false" ? value === false || value : void 0)) {
        return "false";
      }
    }
    return value;
  };

  Radio.prototype.afterRender = function() {
    this.input().on("change", (function(_this) {
      return function() {
        return _this.updateValue();
      };
    })(this));
    return this.setValueOnEl();
  };

  Radio.prototype.readValueFromEl = function() {
    return $("input[name='" + this.getName() + "']:checked", this.options.$el).val();
  };

  Radio.prototype.setValueOnEl = function() {
    return this.input().each((function(_this) {
      return function(i, el) {
        if ($(el).val() === _this.formatValueForView(_this.value)) {
          return $(el).prop('checked', true);
        } else {
          return $(el).prop('checked', false);
        }
      };
    })(this));
  };

  Radio.prototype.input = function() {
    if (!this.options.$el) {
      throw new Error('$el is required');
    }
    return $("input[name='" + this.getName() + "']", this.options.$el);
  };

  Radio.prototype.removeListeners = function() {
    return this.input().unbind();
  };

  return Radio;

})(FormElement);

module.exports = Radio;
