var $, JsonInterfaces, SelectField, UIElement,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

JsonInterfaces = require("../json-interfaces");

UIElement = require("./ui-element");

$ = require('jquery');

SelectField = (function(_super) {
  __extends(SelectField, _super);

  function SelectField(options) {
    if (options == null) {
      options = {};
    }
    if (!options.template) {
      options.template = JsonInterfaces.templates.selectField;
    }
    SelectField.__super__.constructor.call(this, options);
    if (typeof options.options === "function") {
      options.options = options.options();
    }
    this.initRange();
    if (this.options.reverse === true) {
      this.options.options = this.options.options.reverse();
    }
  }

  SelectField.prototype.initRange = function() {
    var i, rangeValues, start, _i, _ref, _ref1;
    if (!this.options.range) {
      return;
    }
    rangeValues = [];
    start = this.options.start || 0;
    for (i = _i = _ref = this.options.range[0], _ref1 = this.options.range[1]; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; i = _ref <= _ref1 ? ++_i : --_i) {
      rangeValues.push({
        value: i + start,
        label: i + start
      });
    }
    return this.options.options = rangeValues;
  };

  SelectField.prototype.updateValue = function() {
    return this.setValue(this.readValueFromEl(), {
      updateDependentValues: false
    });
  };

  SelectField.prototype.updateDependentValues = function(options) {
    if (options == null) {
      options = {};
    }
    return this.setValueOnEl();
  };

  SelectField.prototype.afterRender = function() {
    this.input().on("change", (function(_this) {
      return function() {
        _this.updateValue();
        return _this.ifValid(function() {
          return _this.hideErrors();
        });
      };
    })(this));
    return this.setValueOnEl();
  };

  SelectField.prototype.readValueFromEl = function() {
    return this.input().val();
  };

  SelectField.prototype.setValueOnEl = function() {
    if (this.value) {
      return this.input().val(this.value);
    }
  };

  SelectField.prototype.input = function() {
    if (!this.options.$el) {
      throw new Error('$el is required');
    }
    return $("select", this.options.$el);
  };

  SelectField.prototype.removeListeners = function() {
    return this.input().unbind();
  };

  return SelectField;

})(UIElement);

module.exports = SelectField;
