var $, FormElement, Select, jsoni,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

FormElement = require('../form-element');

$ = require('jquery');

jsoni = require('../jsoni');

Select = (function(_super) {
  __extends(Select, _super);

  function Select(options) {
    options || (options = {});
    if (!options.template) {
      options.template = jsoni.getTemplate('select');
    }
    options.value = options.value || '';
    options.label = options.label || '';
    options.help = options.help || '';
    Select.__super__.constructor.call(this, options);
    if (typeof options.options === "function") {
      options.options = options.options();
    }
    this.initRange();
    if (this.options.reverse === true) {
      this.options.options = this.options.options.reverse();
    }
  }

  Select.prototype.initRange = function() {
    var i, rangeValues, start, _i, _ref, _ref1;
    if (!this.options.range) {
      return;
    }
    rangeValues = [];
    start = this.options.start || 0;
    for (i = _i = _ref = this.options.range[0], _ref1 = this.options.range[1]; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; i = _ref <= _ref1 ? ++_i : --_i) {
      rangeValues.push({
        value: i + start,
        label: i + start
      });
    }
    return this.options.options = rangeValues;
  };

  Select.prototype.afterRender = function() {
    this.input().on("change", (function(_this) {
      return function() {
        return _this.updateValue();
      };
    })(this));
    return this.setValueOnEl();
  };

  Select.prototype.readValueFromEl = function() {
    return this.input().val();
  };

  Select.prototype.setValueOnEl = function() {
    if (this.value) {
      return this.input().val(this.value);
    }
  };

  Select.prototype.input = function() {
    if (!this.options.$el) {
      throw new Error('$el is required');
    }
    return $("select", this.options.$el);
  };

  Select.prototype.removeListeners = function() {
    return this.input().unbind();
  };

  return Select;

})(FormElement);

module.exports = Select;
