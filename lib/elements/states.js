var SelectField, StatesField,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

SelectField = require("./select-field");

StatesField = (function(_super) {
  __extends(StatesField, _super);

  function StatesField(options) {
    options.options = this.states();
    StatesField.__super__.constructor.call(this, options);
  }

  StatesField.prototype.states = function() {
    return [
      {
        value: "AL",
        label: "Alabama"
      }, {
        value: "AK",
        label: "Alaska"
      }, {
        value: "AZ",
        label: "Arizona"
      }, {
        value: "AR",
        label: "Arkansas"
      }, {
        value: "CA",
        label: "California"
      }, {
        value: "CO",
        label: "Colorado"
      }, {
        value: "CT",
        label: "Connecticut"
      }, {
        value: "DE",
        label: "Delaware"
      }, {
        value: "DC",
        label: "District Of Columbia"
      }, {
        value: "FL",
        label: "Florida"
      }, {
        value: "GA",
        label: "Georgia"
      }, {
        value: "HI",
        label: "Hawaii"
      }, {
        value: "ID",
        label: "Idaho"
      }, {
        value: "IL",
        label: "Illinois"
      }, {
        value: "IN",
        label: "Indiana"
      }, {
        value: "IA",
        label: "Iowa"
      }, {
        value: "KS",
        label: "Kansas"
      }, {
        value: "KY",
        label: "Kentucky"
      }, {
        value: "LA",
        label: "Louisiana"
      }, {
        value: "ME",
        label: "Maine"
      }, {
        value: "MD",
        label: "Maryland"
      }, {
        value: "MA",
        label: "Massachusetts"
      }, {
        value: "MI",
        label: "Michigan"
      }, {
        value: "MN",
        label: "Minnesota"
      }, {
        value: "MS",
        label: "Mississippi"
      }, {
        value: "MO",
        label: "Missouri"
      }, {
        value: "MT",
        label: "Montana"
      }, {
        value: "NE",
        label: "Nebraska"
      }, {
        value: "NV",
        label: "Nevada"
      }, {
        value: "NH",
        label: "New Hampshire"
      }, {
        value: "NJ",
        label: "New Jersey"
      }, {
        value: "NM",
        label: "New Mexico"
      }, {
        value: "NY",
        label: "New York"
      }, {
        value: "NC",
        label: "North Carolina"
      }, {
        value: "ND",
        label: "North Dakota"
      }, {
        value: "OH",
        label: "Ohio"
      }, {
        value: "OK",
        label: "Oklahoma"
      }, {
        value: "OR",
        label: "Oregon"
      }, {
        value: "PA",
        label: "Pennsylvania"
      }, {
        value: "RI",
        label: "Rhode Island"
      }, {
        value: "SC",
        label: "South Carolina"
      }, {
        value: "SD",
        label: "South Dakota"
      }, {
        value: "TN",
        label: "Tennessee"
      }, {
        value: "TX",
        label: "Texas"
      }, {
        value: "UT",
        label: "Utah"
      }, {
        value: "VT",
        label: "Vermont"
      }, {
        value: "VA",
        label: "Virginia"
      }, {
        value: "WA",
        label: "Washington"
      }, {
        value: "WV",
        label: "West Virginia"
      }, {
        value: "WI",
        label: "Wisconsin"
      }, {
        value: "WY",
        label: "Wyoming"
      }
    ];
  };

  return StatesField;

})(SelectField);
