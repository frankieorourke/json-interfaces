var $, JsonInterfaces, SubmitButton,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

JsonInterfaces = require('../json-interfaces');

$ = require('jquery');

SubmitButton = (function(_super) {
  __extends(SubmitButton, _super);

  function SubmitButton(options) {
    this.noValue = true;
    options.value = options.value || "Submit";
    if (!options.template) {
      options.template = JsonInterfaces.templates.submitButton;
    }
    SubmitButton.__super__.constructor.call(this, options);
  }

  SubmitButton.prototype.render = function($el) {
    this.setValue(this.options.value, true);
    if ($el) {
      this.options.$el = $el;
    }
    this.options.$el.html(this.options.template({
      options: this.options,
      id: this.elId(),
      namespace: this.options.namespace ? this.options.namespace + "-" : ""
    }));
    return $("#" + this.elId(), this.options.$el).click((function(_this) {
      return function(e) {
        e.preventDefault();
        return _this.submit();
      };
    })(this));
  };

  return SubmitButton;

})(JsonInterfaces.UIElement);

module.exports = SubmitButton;
