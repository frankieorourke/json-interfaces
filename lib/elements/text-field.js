var $, JsonInterfaces, TextField, UIElement,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

UIElement = require('./ui-element');

$ = require('jquery');

JsonInterfaces = require('../json-interfaces');

TextField = (function(_super) {
  __extends(TextField, _super);

  function TextField(options) {
    if (options == null) {
      options = {};
    }
    if (!options.template) {
      options.template = JsonInterfaces.templates.textField;
    }
    TextField.__super__.constructor.call(this, options);
  }

  TextField.prototype.readValueFromEl = function() {
    return $.trim(this.input().val());
  };

  TextField.prototype.setValueOnEl = function() {
    return this.input().val(this.formattedValueForView());
  };

  TextField.prototype.updateValue = function(silent) {
    if (silent == null) {
      silent = false;
    }
    return this.setValue(this.readValueFromEl(), {
      updateDependentValues: false,
      silent: silent
    });
  };

  TextField.prototype.afterRender = function() {
    this.updateDependentValues();
    this.input().on("keyup blur change", (function(_this) {
      return function() {
        return _this.updateValue();
      };
    })(this));
    if (this.options.validateOnBlur !== false) {
      this.input().on("blur", (function(_this) {
        return function() {
          return _this.validate();
        };
      })(this));
    }
    if (this.options.revalidateOnKeyup !== false) {
      return this.input().on("keyup", (function(_this) {
        return function() {
          return _this.ifValid(function() {
            return _this.hideErrors();
          });
        };
      })(this));
    }
  };

  TextField.prototype.updateDependentValues = function() {
    if (!this.rendered) {
      return;
    }
    return this.setValueOnEl();
  };

  TextField.prototype.formatValue = function(value) {
    if (value) {
      return value;
    } else {
      return '';
    }
  };

  TextField.prototype.formattedValueForView = function() {
    return this.value;
  };

  TextField.prototype.input = function() {
    if (!this.options.$el) {
      throw new Error('$el is required');
    }
    return $('input', this.options.$el);
  };

  TextField.prototype.cleanUp = function() {
    return this.input().unbind();
  };

  return TextField;

})(UIElement);

module.exports = TextField;
