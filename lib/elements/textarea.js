var $, JsonInterfaces, TextField, Textarea, UIElement,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

UIElement = require('./ui-element');

$ = require('jquery');

JsonInterfaces = require('../json-interfaces');

TextField = require("./text-field");

Textarea = (function(_super) {
  __extends(Textarea, _super);

  function Textarea(options) {
    if (options == null) {
      options = {};
    }
    if (!options.template) {
      options.template = JsonInterfaces.templates.textarea;
    }
    Textarea.__super__.constructor.call(this, options);
  }

  Textarea.prototype.input = function() {
    if (!this.options.$el) {
      throw new Error('$el is required');
    }
    return $('textarea', this.options.$el);
  };

  return Textarea;

})(TextField);

module.exports = Textarea;
