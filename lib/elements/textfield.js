var $, FormElement, Textfield, jsoni,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

FormElement = require('../form-element.coffee');

$ = require('jquery');

jsoni = require('../jsoni.coffee');

Textfield = (function(_super) {
  __extends(Textfield, _super);

  function Textfield(options) {
    options || (options = {});
    if (!options.template) {
      options.template = jsoni.getTemplate('textfield');
    }
    options.value = options.value || '';
    options.label = options.label || '';
    options.help = options.help || '';
    Textfield.__super__.constructor.call(this, options);
  }

  Textfield.prototype.readValueFromEl = function() {
    return this.input().val();
  };

  Textfield.prototype.setValueOnEl = function() {
    return this.input().val(this.formatValueForView(this.value));
  };

  Textfield.prototype.afterRender = function() {
    this.input().on("blur change keyup", (function(_this) {
      return function() {
        return _this.updateValue();
      };
    })(this));
    if (this.options.validateOnBlur !== false) {
      return this.input().on("blur", (function(_this) {
        return function() {
          return _this.validate();
        };
      })(this));
    }
  };

  Textfield.prototype.input = function() {
    if (!this.options.$el) {
      throw new Error('$el is required');
    }
    return $('input', this.options.$el);
  };

  Textfield.prototype.removeListeners = function() {
    return this.input().unbind();
  };

  return Textfield;

})(FormElement);

module.exports = Textfield;
