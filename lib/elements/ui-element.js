var $, JsonInterfaces, UIElement,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

JsonInterfaces = require('../json-interfaces');

$ = require("jquery");

UIElement = (function(_super) {
  __extends(UIElement, _super);

  function UIElement(options) {
    options || (options = {});
    options.label = options.label || '';
    options.help = options.help || '';
    UIElement.__super__.constructor.call(this, options);
    if (this.options.parentElement) {
      this.options.parentElement.on("afterRender", (function(_this) {
        return function() {
          return _this.checkConditional(true);
        };
      })(this));
    }
  }

  UIElement.prototype.validate = function(callback) {
    if (typeof this.updateValue === "function") {
      this.updateValue(true);
    }
    return this.conditionMet((function(_this) {
      return function(result) {
        if (result === false) {
          return;
        }
        return _this.isValid(function(isValid) {
          if (isValid) {
            _this.hideErrors();
          } else {
            _this.showErrors();
          }
          if (typeof callback === "function") {
            return callback.call(_this, _this.errors);
          }
        });
      };
    })(this));
  };

  UIElement.prototype.showErrors = function() {
    return this.ifInvalid((function(_this) {
      return function() {
        var error, errorEl, _i, _len, _ref, _results;
        _this.input().addClass('error');
        $('.error-wrapper', _this.options.$el).empty();
        _ref = _this.errors;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          error = _ref[_i];
          errorEl = $('<div class="' + JsonInterfaces.settings.errorClasses + '"></div>').html(error);
          errorEl.appendTo($('.error-wrapper', _this.options.$el));
          errorEl.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            return $(this).removeClass('shake animated');
          });
          _results.push(errorEl.addClass('shake animated'));
        }
        return _results;
      };
    })(this));
  };

  UIElement.prototype.hideErrors = function(immediate) {
    if (immediate == null) {
      immediate = false;
    }
    this.input().removeClass('error');
    this.input().closest('.form-group').removeClass('has-error');
    if (immediate) {
      return $('.error-wrapper', this.options.$el).empty();
    } else {
      return $('.error-wrapper', this.options.$el).children().each(function() {
        return $(this).slideUp(function() {
          return $(this).remove();
        });
      });
    }
  };

  UIElement.prototype.updateDependentValues = function() {};

  UIElement.prototype.elId = function() {
    var namespace;
    namespace = this.options.namespace ? this.options.namespace + "-" : "";
    return this.options.id || namespace + this.options.name;
  };

  UIElement.prototype.close = function() {
    if (typeof this.cleanUp === "function") {
      this.cleanUp();
    }
    return UIElement.__super__.close.apply(this, arguments);
  };

  return UIElement;

})(JsonInterfaces.Element);

module.exports = UIElement;
