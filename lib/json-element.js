var JsonElement, JsonInterfaces, async,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

JsonInterfaces = require('./json-interfaces');

async = require('async');

JsonElement = (function(_super) {
  __extends(JsonElement, _super);

  JsonElement.prototype.ignoreElementChangeEvents = false;

  JsonElement.prototype.errors = [];

  JsonElement.prototype.errorsKeyed = {};

  function JsonElement(options) {
    if (options == null) {
      options = {};
    }
    options.setValue = false;
    JsonElement.__super__.constructor.call(this, options);
    if (!this.options.elements) {
      throw new Error("elements required");
    }
    this.elements = this.buildElements(this.options.elements);
    this.setValue(this.options.value);
    if (!this.options.layout) {
      this.options.layout = JsonInterfaces.layouts.oneColumnLayout;
    }
    this.updateValueFromElements(true);
    this.updateDependentValues();
    this.initChildElementEvents();
    if (this.options.parentElement) {
      this.options.parentElement.on("afterRender", (function(_this) {
        return function() {
          return _this.checkConditional(true);
        };
      })(this));
    }
  }

  JsonElement.prototype.defaultValue = function() {
    return this.options.defaultValue || {};
  };

  JsonElement.prototype.render = function($el) {
    var config;
    if (typeof this.beforeRender === "function") {
      this.beforeRender();
    }
    if ($el) {
      this.options.$el = $el;
    }
    if (!this.options.$el) {
      throw new Error('@options.$el not set');
    }
    config = {
      element: this,
      elements: this.getElements(),
      $el: this.options.$el
    };
    if (this.options.template) {
      config.template = this.options.template;
    }
    this.layout = new this.options.layout(config);
    this.layout.render();
    this.rendered = true;
    if (typeof this.afterRender === "function") {
      this.afterRender();
    }
    return this.trigger("afterRender");
  };

  JsonElement.prototype.buildElements = function(elements) {
    var builtElements, element, elementConfig, name;
    builtElements = {};
    for (name in elements) {
      elementConfig = elements[name];
      if (!(elementConfig instanceof JsonInterfaces.Element)) {
        elementConfig.name = name;
        elementConfig.parentElement = this;
        elementConfig.namespace = this.options.namespace ? this.options.namespace : this.options.name;
        if (!elementConfig.type) {
          elementConfig.type = JsonInterfaces.elements.TextField;
        }
        element = new elementConfig.type(elementConfig);
      }
      builtElements[name] = element || elementConfig;
      element = null;
    }
    return builtElements;
  };

  JsonElement.prototype.initChildElementEvents = function() {
    var element, i, self, _ref, _results;
    self = this;
    _ref = this.elements;
    _results = [];
    for (i in _ref) {
      element = _ref[i];
      element.on("change", function(changed, value) {
        if (self.ignoreElementChangeEvents) {
          return;
        }
        self.value[this.getName()] = value;
        changed = {};
        changed[this.getName()] = value;
        return self.trigger("change", changed, self.value);
      });
      element.on("interfaceError", (function(_this) {
        return function(errors) {
          return _this.trigger("interfaceError", errors);
        };
      })(this));
      _results.push(element.on("submit", (function(_this) {
        return function() {
          return _this.submit();
        };
      })(this)));
    }
    return _results;
  };

  JsonElement.prototype.submit = function(callback) {
    if (this.options.parentElement || this.options.validateOnSubmit === false) {
      this.trigger("submit");
      if (typeof callback === "function") {
        return getValue((function(_this) {
          return function(value) {
            return callback.call(_this, value);
          };
        })(this));
      }
    } else {
      return this.validateIsValid((function(_this) {
        return function() {
          _this.trigger("submit");
          if (typeof callback === "function") {
            return getValue(function(value) {
              return callback.call(_this, value);
            });
          }
        };
      })(this));
    }
  };

  JsonElement.prototype.getValue = function(fresh, callback) {
    var element, name, valueCallback, valueCallbacks, _ref;
    if (fresh == null) {
      fresh = false;
    }
    if (typeof callback !== "function") {
      callback = fresh;
      fresh = false;
    }
    if (fresh) {
      valueCallbacks = [];
      _ref = this.elements;
      for (name in _ref) {
        element = _ref[name];
        if (element.noValue) {
          continue;
        }
        valueCallback = ((function(_this) {
          return function(name, element) {
            return function(asyncCallback) {
              element.updateValue();
              return element.getValue(function(value) {
                var keyed;
                keyed = {};
                keyed[name] = value;
                return asyncCallback(null, keyed);
              });
            };
          };
        })(this))(name, element);
        valueCallbacks.push(valueCallback);
      }
      return async.parallel(valueCallbacks, (function(_this) {
        return function(err, results) {
          var formatted, key, result, value, _i, _len;
          formatted = {};
          for (_i = 0, _len = results.length; _i < _len; _i++) {
            result = results[_i];
            for (key in result) {
              value = result[key];
              formatted[key] = value;
            }
          }
          return callback.call(_this, formatted);
        };
      })(this));
    } else {
      return callback.call(this, this.value);
    }
  };

  JsonElement.prototype.getElement = function(name, valueElementsOnly) {
    if (valueElementsOnly == null) {
      valueElementsOnly = false;
    }
    if (!this.elements[name]) {
      return null;
    }
    if (!(this.elements[name].noValue && valueElementsOnly)) {
      return this.elements[name];
    }
  };

  JsonElement.prototype.getElements = function(valueElementsOnly) {
    var element, name, valueEls, _ref;
    if (valueElementsOnly == null) {
      valueElementsOnly = false;
    }
    if (valueElementsOnly !== true) {
      return this.elements;
    }
    valueEls = {};
    _ref = this.elements;
    for (name in _ref) {
      element = _ref[name];
      if (element.noValue) {
        continue;
      }
      valueEls[name] = element;
    }
    return valueEls;
  };

  JsonElement.prototype.clearErrors = function() {
    var element, name, _ref, _results;
    _ref = this.elements;
    _results = [];
    for (name in _ref) {
      element = _ref[name];
      if (element.noValue) {
        continue;
      }
      _results.push(element.clearErrors());
    }
    return _results;
  };

  JsonElement.prototype.getErrors = function(callback) {
    var element, name, validators, _ref;
    validators = [];
    _ref = this.elements;
    for (name in _ref) {
      element = _ref[name];
      validators.push((function(element) {
        return function(callback) {
          return element.conditionMet(function(result) {
            var keyed;
            keyed = {};
            keyed[element.getName()] = null;
            if (result === false) {
              return callback(null, keyed);
            }
            return element.getErrors(function(elementErrors) {
              keyed[element.getName()] = elementErrors || [];
              return callback(null, keyed);
            });
          });
        };
      })(element));
    }
    this.errors = [];
    this.errorsKeyed = {};
    return async.parallel(validators, (function(_this) {
      return function(err, results) {
        var elementErrors, error, flattened, _i, _j, _len, _len1, _ref1, _ref2;
        flattened = {};
        for (_i = 0, _len = results.length; _i < _len; _i++) {
          error = results[_i];
          flattened = _.extend(flattened, error);
        }
        _this.errorsKeyed = flattened;
        _ref1 = _this.errorsKeyed;
        for (name in _ref1) {
          elementErrors = _ref1[name];
          _ref2 = elementErrors || [];
          for (_j = 0, _len1 = _ref2.length; _j < _len1; _j++) {
            error = _ref2[_j];
            _this.errors.push(error);
          }
        }
        return callback.call(_this, _this.errors);
      };
    })(this));
  };

  JsonElement.prototype.validate = function(callback) {
    return this.conditionMet((function(_this) {
      return function(result) {
        if (result === false) {
          return;
        }
        return _this.getErrors(function() {
          _this.showElementErrors();
          if (typeof callback === "function") {
            return callback.call(_this, _this.errors);
          }
        });
      };
    })(this));
  };

  JsonElement.prototype.showElementErrors = function() {
    var element, name, _ref, _results;
    _ref = this.elements;
    _results = [];
    for (name in _ref) {
      element = _ref[name];
      if (element.noValue) {
        continue;
      }
      _results.push(element.showErrors());
    }
    return _results;
  };

  JsonElement.prototype.validateElements = function() {
    var element, name, _ref, _results;
    _ref = this.elements;
    _results = [];
    for (name in _ref) {
      element = _ref[name];
      if (element.noValue) {
        continue;
      }
      _results.push(element.validate());
    }
    return _results;
  };

  JsonElement.prototype.updateDependentValues = function(options) {
    var element, name, value, _ref;
    if (options == null) {
      options = {};
    }
    if (!options.silent) {
      options.silent = false;
    }
    this.ignoreElementChangeEvents = true;
    _ref = this.value;
    for (name in _ref) {
      value = _ref[name];
      element = this.getElement(name);
      if (!element) {
        continue;
      }
      if (element.noValue) {
        continue;
      }
      if (element) {
        element.setValue(value, options.silent);
      }
    }
    return this.ignoreElementChangeEvents = false;
  };

  JsonElement.prototype.updateValueFromElements = function(override) {
    var element, name, _ref, _results;
    _ref = this.elements;
    _results = [];
    for (name in _ref) {
      element = _ref[name];
      if (element.noValue) {
        continue;
      }
      _results.push(((function(_this) {
        return function(name, element) {
          return element.getValue(function(value) {
            if (!(_this.value[element.getName()] && override)) {
              return _this.value[element.getName()] = value;
            }
          });
        };
      })(this))(name, element));
    }
    return _results;
  };

  return JsonElement;

})(JsonInterfaces.Element);

module.exports = JsonElement;
