var Element, JsonInterfaces;

Element = require('./element');

JsonInterfaces = (function() {
  function JsonInterfaces() {}

  return JsonInterfaces;

})();

JsonInterfaces.Element = Element;

module.exports = JsonInterfaces;

JsonInterfaces.settings = {
  errorClasses: "alert alert-danger"
};

JsonInterfaces.JsonElement = require('./json-element');

JsonInterfaces.UIElement = require('./elements/ui-element');

JsonInterfaces.elements = {
  TextField: require('./elements/text-field'),
  EmailField: require('./elements/email-field'),
  Address: require('./elements/address'),
  PasswordField: require('./elements/password-field'),
  SubmitButton: require('./elements/submit-button'),
  DateField: require('./elements/date-field'),
  Textarea: require('./elements/textarea'),
  Html: require('./elements/html'),
  SelectField: require('./elements/select-field'),
  RadioField: require('./elements/radio-field'),
  StatesField: require('./elements/states-field')
};

JsonInterfaces.layouts = {
  oneColumnLayout: require('./layouts/one-column-layout'),
  templateLayout: require('./layouts/template-layout')
};

JsonInterfaces.templates = {
  oneColumnLayout: require('./templates/one-column-layout'),
  textField: require('./templates/text-field'),
  emailField: require('./templates/email-field'),
  submitButton: require('./templates/submit-button'),
  password: require("./templates/password-field"),
  html: require("./templates/html"),
  dateField: require("./templates/date-field"),
  textarea: require("./templates/textarea"),
  radioField: require("./templates/radio-field"),
  selectField: require("./templates/select-field"),
  address: require("./templates/address")
};

JsonInterfaces.buildInterface = function(options) {
  var elementClass;
  elementClass = options.element || JsonInterfaces.JsonElement;
  if (options.element) {
    delete options.element;
  }
  return new elementClass(options);
};
