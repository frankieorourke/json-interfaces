var $, JsonInterfaces, OneColumnLayout;

JsonInterfaces = require('../json-interfaces');

$ = require('jquery');

OneColumnLayout = (function() {
  function OneColumnLayout(options) {
    this.options = options;
    if (!this.options.element.getElements()) {
      throw new Error('fields required');
    }
    this.elements = this.options.element.getElements();
    if (!this.options.template) {
      this.options.template = JsonInterfaces.templates.oneColumnLayout;
    }
  }

  OneColumnLayout.prototype.elementsEl = function() {
    return $("> form.one-column-layout > .elements", this.options.$el);
  };

  OneColumnLayout.prototype.errorsEl = function() {
    return $("> form.one-column-layout > .errors", this.options.$el);
  };

  OneColumnLayout.prototype.render = function($el) {
    var element, name, _ref, _results;
    this.options.element.on("interfaceError", (function(_this) {
      return function(errors) {
        return _this.showErrors(errors);
      };
    })(this));
    this.options.element.on("change", (function(_this) {
      return function() {
        return _this.hideErrors();
      };
    })(this));
    if ($el) {
      this.options.$el = $el;
    }
    if (!this.options.$el) {
      throw new Error('@options.$el not set');
    }
    this.options.$el.html(this.options.template({
      options: this.options || {},
      errors: this.options.errors || {},
      value: this.options.value
    }));
    _ref = this.elements;
    _results = [];
    for (name in _ref) {
      element = _ref[name];
      $el = $('<div></div>').appendTo(this.elementsEl());
      _results.push(element.render($el));
    }
    return _results;
  };

  OneColumnLayout.prototype.hideErrors = function() {
    return this.errorsEl().children().each(function() {
      return $(this).slideUp(200, function() {
        return $(this).remove();
      });
    });
  };

  OneColumnLayout.prototype.showErrors = function(errors) {
    var elementErrors, error, name, _results;
    this.errorsEl().empty();
    _results = [];
    for (name in errors) {
      elementErrors = errors[name];
      _results.push((function() {
        var _i, _len, _results1;
        _results1 = [];
        for (_i = 0, _len = elementErrors.length; _i < _len; _i++) {
          error = elementErrors[_i];
          _results1.push($('<div class="alert alert-danger">' + error + '</div>').hide().appendTo(this.errorsEl()).slideDown());
        }
        return _results1;
      }).call(this));
    }
    return _results;
  };

  OneColumnLayout.prototype.close = function() {
    var element, _i, _len, _ref;
    _ref = this.elements;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      element = _ref[_i];
      element.close();
    }
    return this.options.$el.empty();
  };

  return OneColumnLayout;

})();

module.exports = OneColumnLayout;
