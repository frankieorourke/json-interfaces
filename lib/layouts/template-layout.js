var $, JsonInterfaces, TemplateLayout;

JsonInterfaces = require('../json-interfaces');

$ = require('jquery');

TemplateLayout = (function() {
  function TemplateLayout(options) {
    this.options = options;
    if (!this.options.elements) {
      throw new Error('elements required');
    }
    if (typeof this.options.template === "undefined") {
      throw new Error('template required');
    }
    this.elements = this.options.elements;
  }

  TemplateLayout.prototype.render = function($el) {
    var element, name, _ref, _results;
    if ($el) {
      this.options.$el = $el;
    }
    if (!this.options.$el) {
      throw new Error('@options.$el not set');
    }
    this.options.$el.html(this.options.template({
      options: this.options || {}
    }));
    _ref = this.elements;
    _results = [];
    for (name in _ref) {
      element = _ref[name];
      _results.push(element.render($("#" + this.toSnakeCase(element.getName()) + "-region", this.options.$el)));
    }
    return _results;
  };

  TemplateLayout.prototype.toSnakeCase = function(str) {
    return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
  };

  TemplateLayout.prototype.close = function() {
    var element, _i, _len, _ref;
    _ref = this.elements;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      element = _ref[_i];
      element.close();
    }
    return this.options.$el.empty();
  };

  return TemplateLayout;

})();

module.exports = TemplateLayout;
