var jade = require("jade/runtime");
function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (options, namespace) {
buf.push("<div class=\"form-group\">");
if ( options.label)
{
buf.push("<label" + (jade.attr("for", "" + (namespace) + "" + (options.name) + "", true, false)) + ">" + (jade.escape((jade_interp = options.label) == null ? '' : jade_interp)) + "&nbsp;&nbsp;</label>");
}
if ( options.help)
{
buf.push("<p class=\"help-block\">" + (jade.escape((jade_interp = options.help) == null ? '' : jade_interp)) + "</p>");
}
// iterate options.choices
;(function(){
  var $$obj = options.choices;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var choice = $$obj[$index];

buf.push("<label class=\"radio-inline\"><input type=\"radio\"" + (jade.attr("name", "" + (namespace) + "" + (options.name) + "", true, false)) + (jade.attr("value", "" + (choice.value) + "", true, false)) + "/><span>" + (jade.escape((jade_interp = choice.label) == null ? '' : jade_interp)) + "</span></label>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var choice = $$obj[$index];

buf.push("<label class=\"radio-inline\"><input type=\"radio\"" + (jade.attr("name", "" + (namespace) + "" + (options.name) + "", true, false)) + (jade.attr("value", "" + (choice.value) + "", true, false)) + "/><span>" + (jade.escape((jade_interp = choice.label) == null ? '' : jade_interp)) + "</span></label>");
    }

  }
}).call(this);

buf.push("<div class=\"error-wrapper\"></div></div>");}.call(this,"options" in locals_for_with?locals_for_with.options:typeof options!=="undefined"?options:undefined,"namespace" in locals_for_with?locals_for_with.namespace:typeof namespace!=="undefined"?namespace:undefined));;return buf.join("");
}
module.exports = template;