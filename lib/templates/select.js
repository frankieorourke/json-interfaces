var jade = require("jade/runtime");
function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (options, errors) {
buf.push("<div class=\"form-group\">");
if ( options.label)
{
buf.push("<label" + (jade.attr("for", "" + (options.name) + "", true, false)) + ">" + (jade.escape((jade_interp = options.label) == null ? '' : jade_interp)) + "&nbsp;&nbsp;</label>");
}
if ( options.help)
{
buf.push("<p class=\"help-block\">" + (jade.escape((jade_interp = options.help) == null ? '' : jade_interp)) + "</p>");
}
buf.push("<select" + (jade.attr("name", "" + (options.name) + "", true, false)) + (jade.attr("id", "" + (options.id || options.name) + "", true, false)) + " class=\"form-control\">");
if ( options.placeholder)
{
buf.push("<option disabled=\"disabled\" selected=\"selected\">" + (jade.escape((jade_interp = options.placeholder) == null ? '' : jade_interp)) + "</option>");
}
// iterate options.options
;(function(){
  var $$obj = options.options;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var option = $$obj[$index];

buf.push("<option" + (jade.attr("value", "" + (option.value) + "", true, false)) + ">" + (jade.escape((jade_interp = option.label) == null ? '' : jade_interp)) + "</option>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var option = $$obj[$index];

buf.push("<option" + (jade.attr("value", "" + (option.value) + "", true, false)) + ">" + (jade.escape((jade_interp = option.label) == null ? '' : jade_interp)) + "</option>");
    }

  }
}).call(this);

buf.push("</select>");
if ( errors.length)
{
buf.push("<div class=\"alert alert-danger form-error\"><ul>");
// iterate errors
;(function(){
  var $$obj = errors;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var error = $$obj[$index];

buf.push("<li>" + (jade.escape((jade_interp = error) == null ? '' : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var error = $$obj[$index];

buf.push("<li>" + (jade.escape((jade_interp = error) == null ? '' : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div>");
}
buf.push("</div>");}.call(this,"options" in locals_for_with?locals_for_with.options:typeof options!=="undefined"?options:undefined,"errors" in locals_for_with?locals_for_with.errors:typeof errors!=="undefined"?errors:undefined));;return buf.join("");
}
module.exports = template;