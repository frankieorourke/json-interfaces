var jade = require("jade/runtime");
function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
;var locals_for_with = (locals || {});(function (options, namespace, value) {
buf.push("<div class=\"form-group\">");
if ( options.label)
{
buf.push("<label" + (jade.attr("for", "" + (namespace) + "" + (options.name) + "", true, false)) + ">" + (jade.escape((jade_interp = options.label) == null ? '' : jade_interp)) + "</label>");
}
if ( options.help)
{
buf.push("<p class=\"help-block\">" + (jade.escape((jade_interp = options.help) == null ? '' : jade_interp)) + "</p>");
}
buf.push("<input type=\"text\"" + (jade.attr("name", "" + (namespace) + "" + (options.name) + "", true, false)) + (jade.attr("id", "" + (options.id || namespace + options.name) + "", true, false)) + (jade.attr("value", "" + (value) + "", true, false)) + " class=\"form-control\"/><div class=\"error-wrapper\"></div></div>");}.call(this,"options" in locals_for_with?locals_for_with.options:typeof options!=="undefined"?options:undefined,"namespace" in locals_for_with?locals_for_with.namespace:typeof namespace!=="undefined"?namespace:undefined,"value" in locals_for_with?locals_for_with.value:typeof value!=="undefined"?value:undefined));;return buf.join("");
}
module.exports = template;