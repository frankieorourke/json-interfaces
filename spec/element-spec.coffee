assert = expect = require("chai").expect
JsonInterfaces = require("../lib/json-interfaces")
elementFactory = require("./factories/element")
jsonElementFactory = require("./factories/json-element")
sinon = require("sinon")
async = require("async")

describe 'Element', ->
  describe "#getName", ->
    it "returns the name of the element", ->
      expect(elementFactory().getName()).to.eq "test"

  describe "#getValue", ->
    it "returns the element's value", (done)->
      elementFactory().getValue (value)->
        expect(value).to.eq 123
        done()

  describe "#setValue", ->
    it "sets the element's value", (done)->
      element = elementFactory()
      try element.setValue(456) catch e;
      element.getValue (value)->
        expect(value).to.eq 456
        done()

  describe "#isValid", ->
    it "returns true if errors are present", (done)->
      element = elementFactory()
      element.errors = ["this is an error"]

      element.isValid (result)->
        expect(result).to.eq false
        done()

    it "returns false if errors are present", (done)->
      element = elementFactory()
      element.isValid (result)->
        expect(result).to.eq true
        done()

  describe "#render", ->
    it "throws an error if $el is not set", ->
      element = elementFactory()
      fn = ()->
        element.render()

      expect(fn).to.throw

    it "calls #beforeRender if it exists", ->
      element = elementFactory()
      $el = {html: ->}

      called = false
      element.beforeRender = ->
        called = true

      element.render($el)
      expect(called).to.eq true

    it "calls #afterRender if it exists", ->
      element = elementFactory()
      $el = {html: ->}

      called = false
      element.afterRender = ->
        called = true

      element.render $el
      expect(called).to.eq true

    it "sets @rendered to true", ->
      element = elementFactory()
      $el = {html: ->}

      expect(element.rendered).to.eq false
      element.render($el)
      expect(element.rendered).to.eq true

    it "sets the html on $el if options.template is defined", ->
      $el = {html: ->}
      mock = sinon.mock($el)
      mock.expects("html").once()
      element = elementFactory
        template: -> #fake template

      element.render($el)
      mock.verify()

    it "passes an object with the @value, @options, and @ to options.template if options.template defined", (done)->
      $el = {html: ->}

      template = ()->
      spy = sinon.spy(template)
      template()

      element = elementFactory
        template: spy

      element.getValue (value)->

        args =
          element: element
          options: element.options
          value: value

        element.render($el)

        expect(spy.withArgs(args).calledOnce).to.eq true
        done()

  describe "#initConditional", ->
    it "converts json config to conditional validation function", (done)->
      elements =
        e1:
          type: JsonInterfaces.Element
          conditional:
            e2: "two"
        e2:
          type: JsonInterfaces.Element
          value: "two"
          conditional:
            e3: "two"
        e3:
          type: JsonInterfaces.Element
          value: "three"

      jsonElement = jsonElementFactory
        elements: elements

      callbacks = []

      #e1 should pass
      callbacks.push (callback)->
        jsonElement.getElement("e1").conditionMet (result)->
          expect(result).to.eq true
          callback()

      #e2 should fail
      callbacks.push (callback)->
        jsonElement.getElement("e2").conditionMet (result)->
          expect(result).to.eq false
          callback()

      async.parallel callbacks, ->
        done()

  describe "#getErrors", ->
    it "returns an array of errors", (done)->
      element = elementFactory
        validation: (value, validator, parentElement, done)->
          done(["Error!"])

      element.getErrors (errors)->
        expect(errors[0]).to.eq "Error!"
        done()

    it "returns an array of errors when there are multiple validators", (done)->
      element = elementFactory
        validation: [
          (value, validator, parentElement, done)->
            done(["Error 1"])
          ,
          (value, validator, parentElement, done)->
            done(["Error 2"])
          ]

      element.getErrors (errors)->
        expect(errors[0]).to.eq "Error 1"
        expect(errors[1]).to.eq "Error 2"
        done()

    it "returns an empty array when there are no errors", (done)->
      element = elementFactory
        validation: [
          (value, validator, parentElement, done)->
            done()
        ,
          (value, validator, parentElement, done)->
            done()
        ]

      element.getErrors (errors)->
        expect(errors.length).to.eq 0
        done()

    it "returns an array of errors when validators return string values", (done)->
      element = elementFactory
        validation: [
          (value, validator, parentElement, done)->
            done("Error 1")
        ,
          (value, validator, parentElement, done)->
            done(["Error 2"])
        ]

      element.getErrors (errors)->
        expect(errors[0]).to.eq "Error 1"
        expect(errors[1]).to.eq "Error 2"
        done()

  describe "#initValidation", ->
    it "enforces required: true", (done)->

      elements =
        e1:
          type: JsonInterfaces.Element
          required: true
          value: null
        e2:
          type: JsonInterfaces.Element

      jsonElement = jsonElementFactory
        elements: elements

      jsonElement.isValid (result)->
        expect(result).to.eq false
        done()

    it "required false does what it should", (done)->

      elements =
        e1:
          type: JsonInterfaces.Element
          required: false
          value: null
        e2:
          type: JsonInterfaces.Element

      jsonElement = jsonElementFactory
        elements: elements

      jsonElement.isValid (result)->
        expect(result).to.eq true
        done()