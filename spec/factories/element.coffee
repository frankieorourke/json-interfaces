Element = require('../../lib/element')
_ = require('underscore')

module.exports = (options)->
  factoryOptions =
    name: "test"
    value: 123
    template: ->

  options = _.extend(factoryOptions, options)

  new Element(options)