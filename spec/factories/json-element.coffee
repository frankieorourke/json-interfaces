JsonElement = require('../../lib/json-element')
_ = require('underscore')

module.exports = (options)->
  factoryOptions =
    name: "test"

  options = _.extend(factoryOptions, options)

  new JsonElement(options)