assert = expect = require("chai").expect
JsonInterfaces = require("../lib/json-interfaces")
jsonElementFactory = require("./factories/json-element")
elementFactory = require("./factories/element")
sinon = require("sinon")

describe 'JsonElement', ->
  describe "initialization", ->
    it "requires children elements to be present", ->
      fn = ->
        jsonElementFactory()

      expect(fn).to.throw "elements required"

    it "sets the values first from elements, then from specified values", (done)->
      e1 = elementFactory(name: "e1", value: "e1 val")
      e2 = elementFactory(name: "e2", value: "e2 val")
      e3 = elementFactory(name: "e3", value: "e3 val")
      elements =
        e1: e1
        e2: e2
        e3: e3

      jsonElement = jsonElementFactory
        elements: elements
        value:
          e3: "e3 new val"

      jsonElement.getValue (value)->
        expect(value.e1).to.eq "e1 val"
        expect(value.e2).to.eq "e2 val"
        expect(value.e3).to.eq "e3 new val"
        done()

  describe "#render", ->
    it "creates a layout and passes itself in if options.layout is defined", ->
      args = null

      class Layout
        constructor: (options)->
          args = options
        render: ->

      element = jsonElementFactory
        elements: {}
        layout: Layout

      $el = {html: ->}
      element.render($el)

      expect(args.element).to.eq element
      expect(args.$el).to.eq $el

  describe "#close", ->
    it "closes the layout if its defined", ->
      Layout = ->
      Layout.prototype.render = ->
      Layout.prototype.close = ->
        @closed = true

      element = jsonElementFactory
        elements: {}
        layout: Layout

      element.render({html: ->})
      element.close()
      expect(element.layout.closed).to.eq true

  describe "#getErrors", ->
    it "returns all errors from child elements", (done)->
      e1 = elementFactory(name: "e1")
      e2 = elementFactory(name: "e2")
      elements =
        e1: e1
        e2: e2

      #mock
      e1.getErrors = (callback)->
        callback(["Error One"])

      e2.getErrors = (callback)->
        callback(["Error Two"])

      jsonElement = jsonElementFactory
        elements: elements

      jsonElement.getErrors (errors)->
        expect(errors['e1'][0]).to.eq "Error One"
        expect(errors['e2'][0]).to.eq "Error Two"
        done()

    it "does not get errors from elements whose conditionals are not met", (done)->
      elements =
        e1:
          type: JsonInterfaces.Element
          value: "test 123"
          validation: (value, validator, parentElement, done)->
            done(["error!"])
          conditional: (value, parentElement, done)->
            done(false)

      jsonElement = jsonElementFactory
        elements: elements

      jsonElement.isValid (result)->
        expect(result).to.eq true
        done()

  describe "#validate", ->
    it "calls #validate on call child elements", ->
      e1 = elementFactory(name: "e1")
      mock = sinon.mock(e1)
      mock.expects("validate").once()

      jsonElement = jsonElementFactory
        elements:
          e1: e1

      jsonElement.validate()
      mock.verify()

  describe "#isValid", ->
    it "should return false if child elements fail validation", (done)->
      #mock it
      e1 = elementFactory
        name: "e1"
        validation: (value, validator, parentElement, done)->
          done(["nope!"])

      jsonElement = jsonElementFactory
        elements:
          e1: e1

      jsonElement.isValid (result)->
        expect(result).to.eq false
        done()

  describe "#showErrors", ->
    it "calls show errors on all child elements", ->
      e1 = elementFactory(name: "e1")
      e2 = elementFactory(name: "e2")
      elements =
        e1: e1
        e2: e2

      jsonElement = jsonElementFactory
        elements: elements

      #mock
      m1 = sinon.mock(e1)
      m2 = sinon.mock(e2)
      m1.expects("showErrors")
      m2.expects("showErrors")

      jsonElement.showErrors()

  describe "#setValue", ->
    it "calls setValue for every field whose name matches a key", ->
      e1 = elementFactory(name: "e1")
      e2 = elementFactory(name: "e2")
      elements =
        e1: e1
        e2: e2

      jsonElement = jsonElementFactory
        elements: elements

      m1 = sinon.mock(e1)
      m2 = sinon.mock(e2)
      m1.expects("setValue").withArgs("new e1 val")
      m2.expects("setValue").withArgs("new e2 val")

      jsonElement.setValue
        e1: "new e1 val"
        e2: "new e2 val"

      m1.verify()
      m2.verify()

  describe "#updateDependentValues", ->
    it "ignores change events for child elements while #updateDependentValues is being executed", ->
      e1 = elementFactory(name: "e1")
      e2 = elementFactory(name: "e2")
      elements =
        e1: e1
        e2: e2

      jsonElement = jsonElementFactory
        elements: elements

      mock = sinon.mock(jsonElement)
      mock.expects("trigger").once().withArgs("change")

      jsonElement.setValue
        e1: "new e1 val"
        e2: "new e2 val"

      mock.verify()

  describe "#getElement", ->
    it "returns a child element with the specified name", ->
      e1 = elementFactory(name: "e1")
      e2 = elementFactory(name: "e2")
      elements =
        e1: e1
        e2: e2

      jsonElement = jsonElementFactory
        elements: elements

      expect(jsonElement.getElement("e1")).to.eq e1
      expect(jsonElement.getElement("e2")).to.eq e2

  describe "event:change", ->
    it "triggers a change event when a change events bubbles up from a child element", (done)->
      e1 = elementFactory(name: "e1")
      e2 = elementFactory(name: "e2", value: "e2 val")
      elements =
        e1: e1
        e2: e2

      jsonElement = jsonElementFactory
        elements: elements

      jsonElement.on "change", (changed, value)->
        expect(value.e1).to.eq "test value"
        expect(value.e2).to.eq "e2 val"
        done()

      #changed, value
      e1.trigger "change", "test value", "test value"

  describe "#buildElements", ->
    it "does nothing to existing elements", ->
      e1 = elementFactory(name: "e1")
      e1.propertyThatWillBeRemoveIfThisIsRebuilt = true
      jsonElement = jsonElementFactory
        elements:
          e1: e1

      expect(jsonElement.getElement('e1').propertyThatWillBeRemoveIfThisIsRebuilt).to.eq true

    it "builds elements from json config", (done)->
      elements =
        e1:
          type: JsonInterfaces.Element
          value: "test 123"

      jsonElement = jsonElementFactory
        elements: elements

      expect(jsonElement.getElement('e1').getName()).to.eq "e1"
      jsonElement.getElement('e1').getValue (value)->
        expect(value).to.eq "test 123"
        done()

