assert = expect = require("chai").expect
JsonInterfaces = require("../lib/json-interfaces")
Element = require('../lib/element')

describe 'JsonInterfaces', ->
  describe "service locator", ->
    it "can get services from JsonInterfaces", ->
      expect(JsonInterfaces.Element).to.eq Element