Events = require('./events')
_ = require('underscore')
$ = require('jquery')
async = require('async')
validator = require('validator')

class Element

  errors: []
  rendered: false
  noValue: false
  validator: validator

  constructor: (options = {})->
    embeddedConfig =  if typeof @getConfig is "function" then @getConfig() else {}
    @options = _.extend(embeddedConfig, options)
    @setValue(@options.value) unless @options.setValue is false
    throw new Error("name required") unless @options.name

    @initConditional()
    @initValidation()

  defaultValue: ->
    @options.defaultValue

  initOptions: (options)->
    if @options
      @options = _.extend(@options, options)

  submit: (callback)->
    @trigger "submit"
    return if @noValue

    @getValue (value)=>
      callback.call(@, value) if typeof callback is "function"

  getName: ->
    @options.name

  getValue: (fresh = false, callback)->
    unless typeof callback is "function"
      callback = fresh
      fresh = false

    if fresh
      @updateValue(true)

    callback.call(@, @value)

  getValueForView: (callback)->
    callback.call(@, @value)

  setValue: (value, options = {})->
    value = value || @defaultValue()

    #default to not silent
    options.silent = false unless options.silent

    #format value and make sure the value has actually changed before setting it and firing a change event
    formattedValue = @formatValue(value)
    return if @value is formattedValue

    #set the value
    @value = formattedValue

    #update dependent values
    unless options.updateDependentValues is false or @rendered is not true
      @updateDependentValues(options.silent)

    #trigger change event with changed value
    @trigger("change", @value, @value) unless options.silent
    @value

  updateDependentValues: (options)->

  isValid: (callback)->
    @getErrors =>
      errorCount = 0
      for name,errors of @errors
        errorCount += (if errors then errors.length else 0)

      callback(errorCount is 0)

  ifValid: (callback)->
    callback.call(@) if @errors.length is 0

  ifInvalid: (callback)->
    callback.call(@) if @errors.length > 0

  validateIsValid: (callback)->
    @validate =>
      callback.call(@) if @errors.length is 0

  getErrors: (callback)->
    if @noValue
      return callback.call(@)

    #update the value here before validation because some browsers (Firefox) does not trigger
    #a change event for autofill data.  the user will see a form populated with data, but will
    #submit it and it will be empty.  The elements think that they have no data because their
    #dom elements never alerted them of a change.
    @updateValue()

    if @options.validation.length > 0
      validatorCallbacks = []
      for validator in @options.validation
        validatorCallback = ((validator)=>
          (asyncCallback)=>
            validator @value, @validator, @options.parentElement, (results)=>
              if results instanceof Array
                return asyncCallback(null, results)
              else if results
                return asyncCallback(null, [results])
              else
                return asyncCallback(null, [])
        )(validator)

        validatorCallbacks.push validatorCallback

      @errors = []
      async.parallel validatorCallbacks, (err, results)=>
        for result in results
          @errors = @errors.concat(result)
        callback.call(@, @errors)
    else
      callback.call(@)

  addError: (error)->
    @errors.push error

  validate: (callback)->
    throw new Error('Not Implemented')

  showErrors: ->
    #not implemented

  hideErrors: ->
    #not implemented

  clearErrors: ->
    @hideErrors(true)
    @errors = []

  render: ($el)->
    @beforeRender() if typeof @beforeRender is "function"
    @options.$el = $el if $el
    throw new Error('@options.$el not set') unless @options.$el
    throw new Error('template required') unless typeof @options.template is "function"

    @getValueForView (value)->
      @options.$el.html @options.template
        value: value
        options: @options
        element: @
        namespace: if @options.namespace then @options.namespace + "-" else ""

    @rendered = true

    #update dom or whatever
    @updateDependentValues(silent: true)

    @afterRender()
    @trigger "afterRender"

  updateValue: (silent = false)->
    #refresh the value from wherever its pulling from (dom, etc)

  afterRender: ->
    #for hooking in

  close: ->
    @layout.close() if @layout

  #protected
  formatValue: (value)->
    value

  conditionMet: (callback)->
    return callback(null) unless typeof @options.conditional is "function"
    @options.conditional @value, @options.parentElement, (result)=>
      callback(result)

  checkConditional: (immediate = false)->
    @conditionMet (result)=>
      if result is false
        @hideElement immediate, =>
          @clearErrors()
      else
        @showElement()

  showElement: (callback)->
    return unless @options.$el

    return if @showing

    @showing = true
    @hiding = false
    @options.$el.stop().slideDown "slow", =>
      @options.$el.css('height', 'auto')
      @showing = false
      callback.call(@) if typeof callback is "function"

  hideElement: (immediate, callback)->
    callback = immediate if typeof immediate is "function"
    return unless @options.$el

    #doesn't have to be visible to hide
    if immediate
      @options.$el.hide()
      @hiding = false
      return callback.call(@) if typeof callback is "function"

    return if @hiding

    @hiding = true
    @showing = false
    @options.$el.stop().slideUp "slow", =>
      @options.$el.css('height', 'auto')
      @hiding = false
      callback.call(@) if typeof callback is "function"

  #Interpret conditional config and map it to the API
  initConditional: ->
    #listen for changes in other elements
    if @options.parentElement
      @options.parentElement.on "change", =>
        @checkConditional()

    return unless typeof @options.conditional is "object"

    #top level elements should not be conditional
    return unless @options.parentElement

    conditionalValues = @options.conditional
    @options.conditional = (value, parentElement, done)->
      result = true
      checks = []
      for i,v of conditionalValues
        check = ((i,v)->
          (callback)->
            parentElement.getElement(i).getValue (value)->
              callback(null, value is v)
        )(i,v)

        checks.push check

      async.parallel checks, (err, results)->
        for result in results
          return done(false) if result is false
        done(true)

  #Interpret validation config and map it to the API
  initValidation: ->
    #normalize validation to an array
    if typeof @options.validation is "function"
      @options.validation = [@options.validation]
    else if not @options.validation
      @options.validation = []

    return unless @options.required

    #set up some shorthand for required: true
    message = if @options.required is true then "This field is required." else @options.required
    #validation with conditionals
    @options.validation.push (value, validator, parentElement, done)=>
      done([message] if (value is "" or value is null or value is undefined))

_.extend(Element.prototype, Events)
module.exports = Element