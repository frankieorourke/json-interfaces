$ = require('jquery')
JsonInterfaces = require('../json-interfaces')
_ = require('underscore')

class Address extends JsonInterfaces.JsonElement

  constructor: (options)->
    options.layout = JsonInterfaces.layouts.templateLayout unless options.layout
    options.elements ||= {}
    options.template = JsonInterfaces.templates.address
    options.namespace = options.namespace + "-" + options.name
    super options

    #Elements
    _.extend options.elements,
      address:
        label: "Street Address"
        required: true
      city:
        label: "City"
        required: true
      state:
        type: JsonInterfaces.elements.StatesField
        label: "State"
        required: true
      zip:
        label: "Zip"
        required: true

    super options

module.exports = Address
