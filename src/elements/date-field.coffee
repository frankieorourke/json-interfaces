JsonInterfaces = require("../json-interfaces")
TextField = require("./text-field")
require('date-utils')

class DateField extends TextField

  constructor: (options)->
    options.template = JsonInterfaces.templates.dateField unless options.template
    super options

  setValue: (value, options = {})->
    super(@stringToDate(value), options)

  formatValue: (value)->
    @stringToDate(value)

  formattedValueForView: ->
    return '' unless @value
    @value.toFormat("YYYY-MM-DD")

  stringToDate: (value)->
    if typeof value is "string"
      date = new Date(value)
      value = if isNaN(date) then null else date
    value

  setValueOnEl: ->
    @input().attr("value", @formattedValueForView())

module.exports = DateField