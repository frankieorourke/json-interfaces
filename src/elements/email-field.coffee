JsonInterfaces = require("../json-interfaces")
TextField = require("./text-field")

class EmailField extends TextField

  constructor: (options)->
    options.template = JsonInterfaces.templates.emailField unless options.template
    super options

    @options.validation.push (value, validator, parentElement, done)->
      done("Valid email required." unless validator.isEmail(value))

module.exports = EmailField