JsonInterfaces = require("../json-interfaces")
Element = require("../element")

class Html extends Element

  noValue: true

  constructor: (options)->
    @options = options
    @options.html = options.html
    @options.template = JsonInterfaces.templates.html

  getName: ->
    @options.name

  render: ($el)->
    @options.$el = $el if $el

    throw new Error('@options.$el not set') unless @options.$el

    @options.$el.html @options.template
      options: @options

    @afterRender() if typeof @afterRender is "function"
    @trigger "afterRender"

  close: ->
    @options.$el.empty()

  addClass: ->
  removeClass: ->

module.exports = Html