TextField = require("./text-field")
passwordTemplate = require("../templates/password-field")

class PasswordField extends TextField

  constructor: (options = {})->
    options.template = passwordTemplate unless options.template
    super options

  #prepare: (data, method)->
  #  delete data.password if method is 'patch' and !data.password
  #  @options.prepare(data, method) if typeof @options.prepare is "function"

module.exports = PasswordField