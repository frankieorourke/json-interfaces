JsonInterfaces = require("../json-interfaces")
UIElement = require("./ui-element")
$ = require('jquery')

class RadioField extends UIElement

  constructor: (options = {})->
    options.template = JsonInterfaces.templates.radioField unless options.template
    super options
    throw new Error("Choices required for field: " + @getName()) unless @options.choices || @options.boolean
    if @options.boolean is true
      @options.value = @formatValue(@options.value)

    @initChoices()

  initChoices: ->
    if @options.boolean is true
      @options.choices = [{label: "Yes", value: "true"}, {label: "No", value: "false"}]
      return

    return unless typeof @options.choices is "string"
    formattedChoices = []
    choices = @options.choices.split(',')
    for choice in choices
      parts = choice.split(':')
      formattedChoices.push {label: parts[0], value: parts[1]}

    @options.choices = formattedChoices

  formatValue: (value)->
    if @options.boolean is true
      return true if value is "true"
      return false if value is "false"
    value

  formattedValueForView: (value)->
    if @options.boolean is true
      return "true" if (value is true or value is "true")
      return "false" if (value is false or value if "false")
    value

  updateValue: ->
    @setValue(@readValueFromEl(), updateDependentValues: false)

  updateDependentValues: (options = {})->
    @setValueOnEl()

  afterRender: ->
    @input().on "change", =>
      @updateValue()
      @ifValid =>
        @hideErrors()

  readValueFromEl: ->
    $("input[name='" + @getName() + "']:checked", @options.$el).val()

  setValueOnEl: ->
    @input().each (i, el)=>
      if $(el).val() is @formattedValueForView(@value)
        $(el).prop('checked', true)
      else
        $(el).prop('checked', false)

  input: ->
    throw new Error('$el is required') unless @options.$el
    $("input[name='" + @elId() + "']", @options.$el)

  removeListeners: ->
    @input().unbind()

module.exports = RadioField