JsonInterfaces = require("../json-interfaces")
UIElement = require("./ui-element")
$ = require('jquery')

class SelectField extends UIElement

  constructor: (options = {})->
    options.template = JsonInterfaces.templates.selectField unless options.template
    super options

    #dynamic options, baby.
    options.options = options.options() if typeof options.options is "function"
    @initRange()
    @options.options = @options.options.reverse() if @options.reverse is true

  initRange: ->
    return unless @options.range
    rangeValues = []
    start = @options.start || 0
    for i in [@options.range[0]..@options.range[1]]
      rangeValues.push {value: i + start, label: i + start}
    @options.options = rangeValues

  updateValue: ->
    @setValue(@readValueFromEl(), updateDependentValues: false)

  updateDependentValues: (options = {})->
    @setValueOnEl()

  afterRender: ->
    @input().on "change", =>
      @updateValue()
      @ifValid =>
        @hideErrors()

    @setValueOnEl()

  readValueFromEl: ->
    @input().val()

  setValueOnEl: ->
    @input().val(@value) if @value

  input: ->
    throw new Error('$el is required') unless @options.$el
    $("select", @options.$el)

  removeListeners: ->
    @input().unbind()

module.exports = SelectField