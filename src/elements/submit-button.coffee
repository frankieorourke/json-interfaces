JsonInterfaces = require('../json-interfaces')
$ = require('jquery')

class SubmitButton extends JsonInterfaces.UIElement

  constructor: (options)->
    @noValue = true
    options.value = options.value || "Submit"
    options.template = JsonInterfaces.templates.submitButton unless options.template
    super options

  render: ($el)->
    @setValue(@options.value, true)
    @options.$el = $el if $el
    @options.$el.html @options.template
      options: @options
      id: @elId()
      namespace: if @options.namespace then @options.namespace + "-" else ""

    $("#" + @elId(), @options.$el).click (e)=>
      e.preventDefault()
      @submit()

module.exports = SubmitButton