UIElement = require('./ui-element')
$ = require('jquery')
JsonInterfaces = require('../json-interfaces')

class TextField extends UIElement

  constructor: (options = {})->
    options.template = JsonInterfaces.templates.textField unless options.template
    super options

  readValueFromEl: ->
    $.trim(@input().val())

  setValueOnEl: ->
    @input().val(@formattedValueForView())

  updateValue: (silent = false)->
    #don't update the textfield after its set on the element to prevent a circular settings of values
    @setValue(@readValueFromEl(), updateDependentValues: false, silent: silent)

  afterRender: ->
    @updateDependentValues()

    @input().on "keyup blur change", =>
      @updateValue()

    unless @options.validateOnBlur is false
      @input().on "blur", =>
        @validate()

    #error rendering needs to be fixed
    unless @options.revalidateOnKeyup is false
      @input().on "keyup", =>
        @ifValid =>
          @hideErrors()

  updateDependentValues: ->
    return unless @rendered
    @setValueOnEl()

  formatValue: (value)->
    if value then value else ''

  formattedValueForView: ()->
    @value

  input: ->
    throw new Error('$el is required') unless @options.$el
    $('input', @options.$el)

  cleanUp: ->
    @input().unbind()

module.exports = TextField