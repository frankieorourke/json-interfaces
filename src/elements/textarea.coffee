UIElement = require('./ui-element')
$ = require('jquery')
JsonInterfaces = require('../json-interfaces')
TextField = require("./text-field")

class Textarea extends TextField

  constructor: (options = {})->
    options.template = JsonInterfaces.templates.textarea unless options.template
    super options

  input: ->
    throw new Error('$el is required') unless @options.$el
    $('textarea', @options.$el)

module.exports = Textarea