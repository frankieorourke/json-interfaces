JsonInterfaces = require('../json-interfaces')
$ = require("jquery")

class UIElement extends JsonInterfaces.Element

  constructor: (options)->
    options ||= {}
    options.label = options.label || ''
    options.help = options.help || ''
    super options

    #check conditionals after the parent is rendered
    if @options.parentElement
      @options.parentElement.on "afterRender", =>
        @checkConditional(true)

  validate: (callback)->
    @updateValue(true) if typeof @updateValue is "function"

    @conditionMet (result)=>
      return if result is false
      @isValid (isValid)=>
        if isValid then @hideErrors() else @showErrors()
        callback.call(@, @errors) if typeof callback is "function"

  showErrors: ->
    @ifInvalid =>
      @input().addClass('error')
      $('.error-wrapper', @options.$el).empty()
      for error in @errors
        errorEl = $('<div class="' + JsonInterfaces.settings.errorClasses + '"></div>').html(error) #.hide()
        errorEl.appendTo($('.error-wrapper', @options.$el)) #.slideDown()
        errorEl.one 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', ->
          $(@).removeClass('shake animated')
        errorEl.addClass('shake animated')

  hideErrors: (immediate = false)->
    @input().removeClass('error')
    @input().closest('.form-group').removeClass('has-error')
    if immediate
      $('.error-wrapper', @options.$el).empty()
    else
      $('.error-wrapper', @options.$el).children().each ()->
        $(@).slideUp ->
          $(@).remove()

  updateDependentValues: ->
    #set @value on $el input or whatever

  elId: ->
    namespace = if @options.namespace then @options.namespace + "-" else ""
    @options.id || namespace + @options.name

  close: ->
    @cleanUp() if typeof @cleanUp is "function"
    super

module.exports = UIElement