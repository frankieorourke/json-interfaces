JsonInterfaces = require('./json-interfaces')
async = require('async')

class JsonElement extends JsonInterfaces.Element

  # prevent @ from catching element change events when @ just set a value on the element
  # @see - #updateDependentValues
  ignoreElementChangeEvents: false
  errors: []
  errorsKeyed: {}

  constructor: (options = {})->
    options.setValue = false
    super options
    #@options = _.extend(@options, options)

    throw new Error("elements required") unless @options.elements
    @elements = @buildElements(@options.elements)

    @setValue(@options.value)

    #default to one column layout
    @options.layout = JsonInterfaces.layouts.oneColumnLayout unless @options.layout

    # First, update @value from child elements
    # Second, update child elements based on @value

    # This order is important because it first grabs any values from the child elements which could be explicitly
    # passed into the elements as default values.  It then overrides that value if a value is passed in
    # to the parent element.

    @updateValueFromElements(true)
    @updateDependentValues()

    #listen for events from child elements
    @initChildElementEvents()

    #check conditional after the parent is rendered
    if @options.parentElement
      @options.parentElement.on "afterRender", =>
        @checkConditional(true)

  defaultValue: ->
    @options.defaultValue || {}

  render: ($el)->
    @beforeRender() if typeof @beforeRender is "function"
    @options.$el = $el if $el
    throw new Error('@options.$el not set') unless @options.$el

    config =
      element: @
      elements: @getElements()
      $el: @options.$el

    config.template = @options.template if @options.template
    @layout = new @options.layout(config)
    @layout.render()

    #@setValue(@options.value, {updateDependentValues: false, silent: true, rendered: true})

    @rendered = true

    @afterRender() if typeof @afterRender is "function"
    @trigger "afterRender"

  buildElements: (elements)->
    builtElements = {}
    for name, elementConfig of elements
      #make sure any prebuilt elements don't have a type function!
      unless elementConfig instanceof JsonInterfaces.Element
        elementConfig.name = name
        elementConfig.parentElement = @
        elementConfig.namespace = if @options.namespace then @options.namespace else @options.name
        #default to text element
        elementConfig.type = JsonInterfaces.elements.TextField unless elementConfig.type
        element = new elementConfig.type(elementConfig)
      builtElements[name] = element || elementConfig #ie an already built element
      element = null

    builtElements

  initChildElementEvents: ->
    self = @
    for i,element of @elements

      element.on "change", (changed, value)->
        return if self.ignoreElementChangeEvents
        self.value[@getName()] = value

        #changed value
        changed = {}
        changed[@getName()] = value

        self.trigger "change", changed, self.value

      element.on "interfaceError", (errors)=>
        @trigger "interfaceError", errors

      element.on "submit", =>
        @submit()

  submit: (callback)->
    if @options.parentElement or @options.validateOnSubmit is false
      @trigger "submit"
      if typeof callback is "function"
        getValue (value)=>
          callback.call(@, value)
    else
      @validateIsValid =>
        @trigger "submit"
        if typeof callback is "function"
          getValue (value)=>
            callback.call(@, value)

  getValue: (fresh = false, callback)->
    unless typeof callback is "function"
      callback = fresh
      fresh = false

    if fresh
      valueCallbacks = []
      for name, element of @elements
        continue if element.noValue
        valueCallback = ((name, element)=>
          (asyncCallback)=>
            element.updateValue()
            element.getValue (value)=>
              keyed = {}
              keyed[name] = value
              asyncCallback(null, keyed)
        )(name, element)

        valueCallbacks.push valueCallback

      async.parallel valueCallbacks, (err, results)=>
        formatted = {}

        for result in results
          for key,value of result
            formatted[key] = value

        callback.call(@, formatted)
    else
      callback.call(@, @value)

  getElement: (name, valueElementsOnly = false)->
    #make sure element exists
    #this for values that do not have fields
    return null unless @elements[name]
    @elements[name] unless @elements[name].noValue and valueElementsOnly

  getElements: (valueElementsOnly = false)->
    return @elements unless valueElementsOnly is true

    valueEls = {}
    for name,element of @elements
      continue if element.noValue
      valueEls[name] = element
    valueEls

  clearErrors: ->
    for name, element of @elements
      continue if element.noValue
      element.clearErrors()

  getErrors: (callback)->
    validators = []
    for name,element of @elements
      validators.push ((element)->
        (callback)->
          #do not run validation if this element is "disabled" via its conditional
          element.conditionMet (result)->
            keyed = {}
            keyed[element.getName()] = null
            return callback(null, keyed) if result is false
            element.getErrors (elementErrors)->
              keyed[element.getName()] = elementErrors || []
              callback(null, keyed)
      )(element)

    @errors = []
    @errorsKeyed = {}
    async.parallel validators, (err, results)=>
      flattened = {}
      for error in results
        flattened = _.extend(flattened, error)

      @errorsKeyed = flattened

      #simple list of errors
      for name,elementErrors of @errorsKeyed
        for error in (elementErrors || [])
          @errors.push error

      callback.call(@, @errors)

  validate: (callback)->
    @conditionMet (result)=>
      return if result is false
      @getErrors =>
        @showElementErrors()
        callback.call(@, @errors) if typeof callback is "function"

  showElementErrors: ->
    for name,element of @elements
      continue if element.noValue
      element.showErrors()

  validateElements: ->
    for name,element of @elements
      continue if element.noValue
      element.validate()

  #@override
  #update elements elements, the DOM, etc, depending on the type of element
  updateDependentValues: (options = {})->
    options.silent = false unless options.silent
    @ignoreElementChangeEvents = true
    for name, value of @value
      element = @getElement(name)
      continue unless element
      continue if element.noValue
      element.setValue(value, options.silent) if element
    @ignoreElementChangeEvents = false

  #when init is true, do not override existing values
  updateValueFromElements: (override)->
    for name, element of @elements
      continue if element.noValue
      ((name, element)=>
        element.getValue (value)=>
          @value[element.getName()] = value unless (@value[element.getName()] and override)
      )(name, element)

module.exports = JsonElement