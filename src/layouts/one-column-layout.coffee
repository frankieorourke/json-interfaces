JsonInterfaces = require('../json-interfaces')
$ = require('jquery')

class OneColumnLayout

  constructor: (options)->
    @options = options
    throw new Error('fields required') unless @options.element.getElements()
    @elements = @options.element.getElements()
    @options.template = JsonInterfaces.templates.oneColumnLayout unless @options.template

  elementsEl: ->
    $("> form.one-column-layout > .elements", @options.$el)

  errorsEl: ->
    $("> form.one-column-layout > .errors", @options.$el)

  render: ($el)->
    @options.element.on "interfaceError", (errors)=>
      @showErrors(errors)

    @options.element.on "change", =>
      @hideErrors()

    @options.$el = $el if $el
    throw new Error('@options.$el not set') unless @options.$el
    @options.$el.html @options.template
      options: @options || {}
      errors: @options.errors || {}
      value: @options.value

    #add all the fields to the layout
    for name,element of @elements
      $el = $('<div></div>').appendTo(@elementsEl())
      element.render($el)

  hideErrors: ->
    @errorsEl().children().each ->
      $(@).slideUp 200, ->
        $(@).remove()

  showErrors: (errors)->
    @errorsEl().empty()
    for name, elementErrors of errors
      for error in elementErrors
        $('<div class="alert alert-danger">' + error + '</div>').hide().appendTo(@errorsEl()).slideDown()

  close: ->
    #close all fields
    for element in @elements
      element.close()

    #empty the parent element
    @options.$el.empty()

module.exports = OneColumnLayout