JsonInterfaces = require('../json-interfaces')
$ = require('jquery')

class TemplateLayout

  constructor: (options)->
    @options = options
    throw new Error('elements required') unless @options.elements
    throw new Error('template required') if typeof @options.template is "undefined"
    @elements = @options.elements

  render: ($el)->
    @options.$el = $el if $el
    throw new Error('@options.$el not set') unless @options.$el
    @options.$el.html @options.template
      options: @options || {}

    #add all the fields to the layout
    for name, element of @elements
      element.render( $("#" + @toSnakeCase(element.getName()) + "-region", @options.$el) )

  toSnakeCase: (str)->
    str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase()

  close: ->
    #close all fields
    for element in @elements
      element.close()

    #empty the parent element
    @options.$el.empty()

module.exports = TemplateLayout